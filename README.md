# NetHub


| Projet Nethub                     |
|-----------------------------------|
| Mohammad Djouadar
| Ozcan Asci
| Valentin Claisse
| Esraa Haroun Yahia
| Création d'un Réseau d'Entreprise |
| Superviseur                       |
| Yvan Peter                        |


![](/docs/imgs/schema.png)

# Présentation du Projet 

L’objectif du projet est de mettre en place une infrastructure d’entreprise ayant les caractéristiques suivantes :

— Le réseau comporte une partie publique accessible de l’extérieur (i.e., les autres groupes) et une partie privée pour les machines utilisateurs.

— Chaque groupe hébergera un serveur Web accessible aux autres groupes.

— Les utilisateurs pourront s’échanger des mails y compris d’un groupe à l’autre.

— La partie privée sera sécurisé par un PC sous linux.

— Les adresses de la partie privée seront en adressage privé et ne devront pas être visible à
l’extérieur.

— Les machines de la partie privée appartiennent soit au département comptabilité soit au département production. Les flux de ces différents départements devront être séparés pour assurer la qualité de service du réseau. Toutefois, une communication entre les machines des deux départements doit rester possible.

— De manière à faciliter la vie de l’administrateur et des utilisateurs, les machines utilisateur seront configurées dynamiquement. Les différentes machines utilisateurs ainsi que les serveurs pourront être désignés par un nom.

Si l’ensemble de ces éléments a pu être mis en œuvre avant la fin du projet, on pourra mettre en place les éléments suivants :

— Fournir l’ensemble des services en IPv4 et en IPv6

— Offrir un accès sécurisé depuis un réseau extérieur (VPN).

— Offrir aux utilisateurs un accès Wifi. Le flux correspondant sera séparé des autres flux et permettra uniquement le flux HTTP.

Chaque groupe désignera un membre chargé de la mise en œuvre de l’infrastructure commune permettant de relier les différentes entreprises.

# Remerciements

Nous tenons à remercier :

-   **Yvan Peter** et **Michael Hauspie** : Pour avoir aider et conseiller l'ensemble des équipes.

-   **L'IUT A de Lille** : Pour nous avoir fourni le matériel permettant la réalisation du projet.

# Structure de ce dépôt

* [`configs`](configs) contient les configurations routeurs, switch et certaines informations sur le réseau.
* [`docs`](docs) contient les fichiers de rapport et procédures.

# Détails sur le projet

Plan d'addressage IP :
```
VLAN 2 (Administration) : 192.168.10.254/24

VLAN 3 (Comptabilité) : 192.168.20.254/24  

VLAN 4 (Production) : 192.168.30.254/24
````
- PC Admin. sous Debian : 192.168.10.1
- DHCP (isc-dhcp-server): 192.168.10.12
- DNS (bind9) : 192.168.10.10
- Web (Apache2) : 192.168.10.11
- Firewall (non-utilisé): IPtables -> ACLs
- Mail (Postfix) :
