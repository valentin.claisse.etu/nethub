# Procedure  

## Architecture :

Routeur 1 (Interface GE 0/1) à Routeur 2( Interface GE 0/1)

Switch 1 ( interface GE 0/0 ) à Routeur 1 ( interface GE 0/0 )

Switch 2 ( interface GE 0/0 ) à Routeur 2 ( interface GE 0/0 )



## Switch :



Switching (Version : Switch 2960) : 

Nommer switch 

    hostname S1Adressage VLAN: //à faire pour chaque VLAN(exemple VLAN 3 ci-dessous) : 

        conf t
        (config)interface vlan 3
        (conf-if)ip address 192.168.20.3 255.255.255.0
        exit

Sécurisé avec mot de passe sur switch :

    conf t
    (config)enable password "mot de passe"
    exit

Création VLAN 2:

    (config)vlan 2
    (config-vlan)name Admin
    exit
Adressage VLAN: //à faire pour chaque VLAN(exemple VLAN 3 ci-dessous) : 

        conf t
        (config)interface vlan 3
        (conf-if)ip address 192.168.20.3 255.255.255.0
        exit
Création VLAN 3: 

    (config) vlan 3
    (config-vlan)name Comptabilite
    exit

Création VLAN 4:

    (config)vlan 4
    (config-vlan) name Production
    exit


Organisation interface vlan 2 :

    conf t
    (config)vlan 2
    (config-vlan)interface fa 0/25
    (config-if-range)switchport acces vlan 2
    exit

Organisation interface vlan 3 :

    conf t
    (config)vlan 3
    (config-vlan)interface range fa 0/1 - 12
    (config-if-range)switchport acces vlan 3
    exit

Organisation interface vlan 4 :

    conf t
    (config)vlan 4
    (config-vlan)interface range fa 0/13 - 24
    (config-if-range)switchport acces vlan 4
    exit

Configuration trunk inter-vlan :

    conf t
    (config)interface fa 0/48 1 (Interface GE 0/0) à Routeur
    (config-if) switchport mode trunk
    (config-if)no shutdown
    exit

## Routeur : 

Creation sous-interface VLAN 2 (ROUTEUR)

    enable
    conf t
    (config)interface gig 0/0.3
    (config-if)encapsulation dot1Q 2   
    (config-if)ip address 192.168.10.254 255.255.255.0
    (config-if)no shutdown
    exit


Creation sous-interface VLAN 3 (ROUTEUR)

    enable
    conf t
    (config)interface gig 0/0.1
    (config-if)encapsulation dot1Q 3
    (config-if)ip address 192.168.50.254 255.255.255.0
    (config-if)no shutdown
    exit


Creation sous-interface VLAN 4 (ROUTEUR)

    enable
    conf t
    (config)interface gig 0/0.2
    (config-if)encapsulation dot1Q 4 
    (config-if)ip address 192.168.60.254 255.255.255.0
    (config-if)no shutdown
    exit


Activation interface

    interface giga 0/1  
    (config-if)no shutdown  

Test :

    Ping inter vlan 


## Configuration inter-routage  // procédure à faire sur le routeur 1

    config t
    (config)router rip
    (config-router)version 2 
    (config-router)network 10.0.0.0
    (config-router)network 192.168.10.0
    (config-router)network 192.168.20.0
    (config-router)network 192.168.30.0
    (config-router)exit

    (config)interface gig0/1
    (config-if)ip address 10.0.0.1 255.0.0.0  
    (config-if)no shutdown

## Configuration inter-routage  // procédure à faire sur le routeur 2

     config t
    (config)router rip 
    (config-router)version 2
    (config-router)network 10.0.0.0
    (config-router)network 192.168.40.0
    (config-router)network 192.168.50.0
    (config-router)network 192.168.60.0
    (config-router)exit

    (config)interface gig0/1
    (config-if)ip address 10.0.0.2 255.0.0.0  
    (config-if)no shutdown

