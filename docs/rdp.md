
| Projet Nethub                     |
|-----------------------------------|
| Mohammad Djouadar
| Ozcan Asci
| Valentin Claisse
| Esraa Haroun Yahia
| Création d'un Réseau d'Entreprise |
| Superviseur                       |
| Yvan Peter                        |

# Remerciements

Nous tenons à d'abord remercier :

-   **Yvan Peter** et **Michael Hauspie** : Pour avoir aider et conseiller l'ensemble des équipes.

-   **L'IUT A de Lille** : Pour nous avoir fourni le matériel permettant la réalisation du projet.

# Présentation du Projet

## Introduction

L’objectif du projet est de mettre en place une infrastructure d’entreprise ayant les caractéristiques suivantes :

— Le réseau comporte une partie publique accessible de l’extérieur (i.e., les autres groupes) et une partie privée pour les machines utilisateurs.

— Chaque groupe hébergera un serveur Web accessible aux autres groupes.

— Les utilisateurs pourront s’échanger des mails y compris d’un groupe à l’autre.

— La partie privée sera sécurisé par un PC sous linux.

— Les adresses de la partie privée seront en adressage privé et ne devront pas être visible à
l’extérieur.

— Les machines de la partie privée appartiennent soit au département comptabilité soit au département production. Les flux de ces différents départements devront être séparés pour assurer la qualité de service du réseau. Toutefois, une communication entre les machines des deux départements doit rester possible.

— De manière à faciliter la vie de l’administrateur et des utilisateurs, les machines utilisateur seront configurées dynamiquement. Les différentes machines utilisateurs ainsi que les serveurs pourront être désignés par un nom.

Si l’ensemble de ces éléments a pu être mis en œuvre avant la fin du projet, on pourra mettre en place les éléments suivants :

— Fournir l’ensemble des services en IPv4 et en IPv6

— Offrir un accès sécurisé depuis un réseau extérieur (VPN).

— Offrir aux utilisateurs un accès Wifi. Le flux correspondant sera séparé des autres flux et permettra uniquement le flux HTTP.

Chaque groupe désignera un membre chargé de la mise en œuvre de l’infrastructure commune permettant de relier les différentes entreprises.


# Suivi du projet

## Outils de gestion du projet

### Git

<img src="imgs/git.png" alt="drawing" width="200"/>

GitLab est une plateforme de développement collaborative open source. Elle permet de publier des dépôts de code source et de gérer leurs différentes versions.

Afin de gérer les différentes versions de nos fichiers et l'avancement du projet, nous avons créer un dépot Git pour chaque groupe et lui avons donné un nom d'entreprise fictif pour rester dans le thème du projet.

Pour y accéder et modifier depuis chez nous, nous avons installer Git Bash.

Voici les commandes Git les plus utilisées en général :

| Commande                                         | But                                                         |
|--------------------------------------------------|-------------------------------------------------------------|
| git clone "link"                                 | Clonage du dépot distant en local                           |
| git add "file"                                   | Ajout d'un fichier au dépot local                           |
| git commit -m "msg"                              | Attestation des changements au dépot                        |
| git push                                         | Chargement du dépot local au dépot distant                  |

### Markdown

C'est un langage de balisage simple qui nous permet d'écrire des rapports et/ou procédures. C'est ce que nous utilisons.

### MSProject

Microsoft Project est un logiciel de gestion de projets édité par Microsoft. Il nous permet de planifier le projet ainsi que d'analyser et communiquer les données des projets. Nous utilisons la version présente à la faculté.

### Draw.io

<https://app.diagrams.net/>

Un outil gratuit nous permettant de facilement créer des diagrammes, schémas et autres dessins importants.

## Rapport Hebdomadaire

### Semaine 1 (03/10/2022 - 07/10/2022)
* Découverte du projet
* Création du groupe et dépôt sur Gitlab

### Semaine 2 (10/10/2022 - 14/10/2022)
* Analyse du cahier des charges
* Simulation sur packet tracer
* Attribution des tâches

### Semaine 3 (17/10/2022 – 21/10/2022)
* Configuration switch
* Configuration routeur
* Création VLAN
* Configuration INTER-VLAN
* Configuration INTER-ROUTAGE

### Semaine 4 (24/10/2022 – 30/10/2022)
* Création de machines virtuelles
* Configurations/Modifications switch/routeur
* Suivi et écriture de certaines procédures

### Semaine 5 ( 07/11/2022 – 13/11/2022)
* Configuration des PC utilisés pour les intégrer au réseau
* Installation et configuration du serveur TFTP afin d’importer et d’exporter nos configurations switch / routeur
* Début d’installation du DHCP et DNS sur les machines virtuelles

### Semaine 6 ( 14/11/2022 – 20/11/2022)
* Test de fonctionnement du réseau et des configurations routeur / Switch
* Configuration du DHCP et DNS
* Suivi et écriture des procédures

### Semaine 7 ( 21/11/2022 – 27-11/2022)
* Finalisation du DHCP et test de fonctionnement
* Configuration DNS
* Commencement installation Serveur WEB et mail

### Semaine 8 (28/11/2022 – 03/12/2022)
* Configuration DNS
* Configuration NAT pour l’accès extérieur
* Finalisation Web
* Rédaction du rapport de projet

### Semaine 9 (05/12/2022 – 11/12/2022)

* Résolution et configuration du DNS
* Test de mise en place d’infrastructure avec des PC
* Installation et configuration d'un Firewall IPTables

### Semaine 10 (12/12/2022 – 18/12/2022)

* Validation et test du DNS
* Tentative résolution problème Firewall
    * Choix de ne pas utiliser de "DMZ" (vu avec M. Peter)
* Documentation et résolution du serveur mail
* Finalistaion du rapport de projet

### Semaine 11 (02/01/2023 – 08/01/2023) :
* Préparation de l'oral.


# Schéma explicatif du réseau

![](imgs/schema.png)


Voici le schéma prévu des deux réseau mis en place durant ce projet. Les PCs de la partie privée sont en adressage privé, les interfaces extérieures des routeurs sont publiques.  
Un pare-feu sous Linux devait se trouver sous chaque partie privée du routeur.  
Chaque serveur contient un service web, mail, DHCP et DNS.  

Le switch contient 3 VLANs :

- VLAN Comptabilité
- VLAN Production
- VLAN Administration (contenant les services)

Ces 3 VLANs peuvent communiquer entre eux grâce au routage inter-VLAN.  
Ensuite les 2 réseaux sont reliés via un réseau public.

Ici, on se concentrera sur la partie de gauche.

# Le matériel du projet

Voici le matériel que chaque groupe a utilisé durant ce projet :

-   1 Routeur
-   1 Switch
-   au moins 3 PCs (un serveur, un pare-feu et au moins un dernier utilisateur pour tests)
-   Serveur Web (Apache2)
-   Serveur Mail (Postfix + Dovecot)
-   Serveur DHCP (Isc-dhcp-server)
-   Serveur DNS (Bind9)
- 	Pare-Feu (Iptables)

# Justification des services utilisés

Nous avons préféré utiliser des solutions libres et gratuites en partie du à nos expériences personnelles, leurs performances et leurs documentations/solutions disponible en abondance.

- Pare-feu : Iptables car déjà présent sous Debian et est fortement utilisé.
- Web : Apache2 car déjà utilisé par certaines personnes du groupe et sa forte concentration de solutions en ligne.
- DNS, DHCP et Mail : bind9, isc-dhcp-server & postfix car ils sont plutôt répandus et ont pas mal d'expérience, ce qui fait que l'on peut se fier à ces derniers.

# Réseau IP

## Switch

![](imgs/switch.png)

Pour améliorer la qualité de service du réseau, ce dernier a été séparé en 3 VLANs :

-   **VLAN 2 Administration** : 192.168.10.2
-   **VLAN 3 Comptabilité** : 192.168.20.3
-   **VLAN 4 Production** : 192.168.30.4

On attribue les ports du Switch au différents VLANs :

```
interface FastEthernet0/1
 switchport access vlan 3
 no keepalive
!
interface FastEthernet0/2
 switchport access vlan 3
 no keepalive
!
interface FastEthernet0/17
 switchport access vlan 4
 no keepalive
!
interface FastEthernet0/18
 switchport access vlan 4
 no keepalive
!
interface FastEthernet0/33
 switchport access vlan 2
 no keepalive
!
interface FastEthernet0/48
 switchport trunk encapsulation dot1q
 switchport mode trunk
 no keepalive
!
```

## Routeur

![](imgs/router.png)

Pour qu'il soit accessible depuis l'extérieur, un NAT a été configuré.
Ci-dessous voici la configuration appliquée sur le Routeur pour traduire l'adresse IP privée du serveur vers une adresse publique
pour permettre à l'autre groupe d'y accèder.

| Adresse IP Privée | Adresse IP Publique |
|-------------------|---------------------|
| 192.168.10.254    | 10.0.0.2            |

- **Gig0/0.1 (VLAN 2)** : 192.168.10.254/24
- **Gig0/0.2 (VLAN 3)** : 192.168.20.254/24
- **Gig0/0.3 (VLAN 4)** : 192.168.30.254/24
- **Gig0/1**            : 10.0.0.2/30

```
interface GigabitEthernet0/0.3
 encapsulation dot1Q 4
 ip address 192.168.30.254 255.255.255.0
 ip helper-address 192.168.10.12
 ip nat inside
 ip virtual-reassembly in
 no cdp enable
!
interface GigabitEthernet0/1
 ip address 10.0.0.2 255.0.0.0
 ip nat outside
 ip virtual-reassembly in
 duplex auto
 speed auto
!
ip nat source static 192.168.10.254 10.0.0.2
```

Le Serveur contient le service DHCP qui s'occupe d'attribuer automatiquement aux PC Utilisateurs leurs adresses IP respectives. Comme il y a de l'inter-VLAN, les 2 VLANs autres que le serveur n'avaient pas accès au service DHCP, pour cela une petite configuration a été appliquée sur les sous-interfaces :

Les lignes *ip helper-address xxx* permet de renseigner le serveur DHCP sur le Routeur pour que les VLANs Comptabilité et Production puissent y accéder.


### Relation entre le réseau du Groupe 1 et du Groupe 2

En effet, les deux réseaux devaient impérativement communiquer entre eux. Il faut donc y ajouter un routage. Nous avons choisi un routage dynamique pour sa simplicité.
Nous avons donc utiliser RIP.
Après l'avoir conifgurer comme cela :

```
router rip
 version 2
 network 10.0.0.0
 network 192.168.10.0
 network 192.168.20.0
 network 192.168.30.0
!
```

La table de routage ressemble à cela :

```
Gateway of last resort is not set

      10.0.0.0/8 is variably subnetted, 2 subnets, 2 masks
C        10.0.0.0/8 is directly connected, GigabitEthernet0/1
L        10.0.0.2/32 is directly connected, GigabitEthernet0/1
      192.168.10.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.10.0/24 is directly connected, GigabitEthernet0/0.1
L        192.168.10.254/32 is directly connected, GigabitEthernet0/0.1
      192.168.20.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.20.0/24 is directly connected, GigabitEthernet0/0.2
L        192.168.20.254/32 is directly connected, GigabitEthernet0/0.2
      192.168.30.0/24 is variably subnetted, 2 subnets, 2 masks
C        192.168.30.0/24 is directly connected, GigabitEthernet0/0.3
L        192.168.30.254/32 is directly connected, GigabitEthernet0/0.3
R     192.168.40.0/24 [120/1] via 10.0.0.1, 00:00:07, GigabitEthernet0/1
R     192.168.50.0/24 [120/1] via 10.0.0.1, 00:00:07, GigabitEthernet0/1
R     192.168.60.0/24 [120/1] via 10.0.0.1, 00:00:07, GigabitEthernet0/1
```

### Importation des configurations sur le matériel réel

Pour ne pas monopoliser les routeurs/switchs et laisser d'autre personnes après nous les utiliser, nous sauvegardons chaque config à distance et les ré-implentons à chaque début de séances. Pour cela, on utilise TFTP.

Voici un exemple d'utilisation de TFTP par Cisco sur un routeur :

```
Router#copy tftp running-config
Address or name of remote host []? (host_ip)
Source filename []? backup_cfg_for_my_router
Destination filename [running-config]?
Accessing tftp://ip/backup_cfg_for_my_router...
Loading backup_cfg_for_router from (ip): !
[OK - 1030 bytes]

1030 bytes copied in 9.612 secs (107 bytes/sec)
R2#
```

Notre salle ayant déjà des utilisateurs avec TFTP, il suffit de renseigner leurs adresses IP et un nom de fichier pour récupérer/envoyer les configurations.

# Pare-Feu

![](imgs/firewall.png)

Afin de sécuriser le réseau comme demandé, un pare-feu *IPTables* a été mis en place sur un PC sous Linux.


## Règles d'IPTables

Remise à zéro des règles de base :  

    iptables -F

On active le traffic en localhost

    iptables -A INPUT -i lo -j ACCEPT

    // -A --> pour préciser la chain
    // -i --> -i pour préciser l'interface
    // lo --> interface utilisé pour les communcation hote local

Activation connexion HTTP/SSH/HTTPS

        iptables -A INPUT -p tcp --dport 22 -j ACCEPT
        // 22 pour le port SSH

        iptables -A INPUT -p tcp --dport 80 -j ACCEPT
        // 80 pour le port HTTP

        iptables -A INPUT -p tcp --dport 443 -j ACCEPT
        // 443 pour le port HTTPS

        // -p --> préciser le protocole
        // -dport --> préciser le port concerné

Activation connexion DNS

    iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT

    iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT

    iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT

    iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT


Activation pour serveur mail:


        iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT

        // 25 --> port 25 utilisé pour SMTP(envoie e-mail)

    ----

        iptables -t filter -A INPUT -p tcp --dport 110 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 110 -j ACCEPT

        // 110 --> port 110 utilisé pour  POP3(reception d-email)

    -----

        iptables -t filter -A INPUT -p tcp --dport 143 -j ACCEPT

        iptables -t filter -A OUTPUT -p tcp --dport 143 -j ACCEPT

        // 143 --> port 143 utilisé pour IMAP( stockage des e-mail)

Autorisation PING :

        iptables -t filter -A INPUT -p icmp -j ACCEPT

        iptables -t filter -A OUTPUT -p icmp -j ACCEPT

        // icmp --> protocole de ping
        // - t filter --> table filter qui consiste a indiquer les régles des paquets,port,protocole.

Refuser tout les autres trafics sauf ceux autorisé :

    /!\ Attention: Cette commande est à appliquer en dernière régle sinon blocage de tout le reste./!\

        iptables -A INPUT -j DROP


Cependant, ce pare-feu n'est pas intégré au réseau suite à notre choix de ne pas intégrer de "DMZ". Notre solution de subtitution sont les "ACL".

# ACLs

Les Access Control List permettent de filtrer les accès entre les différents réseaux ou de filtrer les accès au routeur lui même.

## Création de l'ACL

    R2# conf t
    R2(config)# ip access-list extended (nom)

## Intégration des régles dans ACL :

    ip access-list extended entreprise

    permit tcp any 192.168.10.0 0.0.0.255 eq 53

        //-> Régle ACL d'autorisation du protocole TCP sur le port 53(DNS) de l'exterieur vers le réseau 192.168.10.0

    permit tcp any 192.168.40.0 0.0.0.255 eq 80

        //-> Régle ACL d'autorisation du protocole TCP sur le port 80(HTTP) de l'exterieur vers le réseau 192.168.40.0

    permit tcp any 192.168.40.0 0.0.0.255 eq 443

        //-> Régle ACL d'autorisation du protocole TCP sur le port 443(HTTPS) de l'exterieur vers le réseau 192.168.40.0

    permit udp any 192.168.40.0 0.0.0.255 eq 53

        //-> Régle ACL d'autorisation du protocole UDP sur le port 80(DNS) de l'exterieur vers le réseau 192.168.40.0

    permit udp any 192.168.40.0 0.0.0.255 eq 443

        //-> Régle ACL d'autorisation du protocole UDP sur le port 443(HTTPS) de l'exterieur vers le réseau 192.168.40.0

     permit tcp any any eq 25

        //-> Régle ACL d'autorisation du protocole TCP sur le port 25 (SMTP)

     permit tcp any any eq 110

        //-> Régle ACL d'autorisation du protocole TCP sur le port 110 (POP3)

     permit tcp any any eq 143

        //-> Régle ACL d'autorisation du protocole TCP sur le port 143 (IMAP)

      deny icmp any any

    //-> Règle ACL de filtrage du protocole ICMP de l'extérieur vers notre réseau privé.

## Application de l'ACL sur l'interface donnant à l'extérieur:

    R2#conf t
    R2(config)#int GigabitEthernet0/1
    R2(config-if)#ip access-group exterieur out

## Table ACL

```
show access-list "nom"

Extended IP access list entreprise
    10 permit tcp any 192.168.10.0 0.0.0.255 eq domain
    20 permit tcp any 192.168.10.0 0.0.0.255 eq www
    30 permit tcp any 192.168.10.0 0.0.0.255 eq 443
    40 permit udp any 192.168.10.0 0.0.0.255 eq domain
    50 permit udp any 192.168.10.0 0.0.0.255 eq 443
    60 deny icmp any any
    70 permit tcp any any eq smtp
    80 permit tcp any any eq pop3
    90 permit tcp any any eq 143
```

Ainsi, nous avons choisi les ports et protocoles bloqués de l'extérieur vers notre réseau privé.  

# PC Administration

## DHCP

<https://www.isc.org/dhcp/>

![](imgs/isc.jpg)

Pour respecter le cahier des charges, un DHCP a été mis en place afin d'attribuer automatiquement aux PC clients des adresses IP. Pour cela, nous avons utilisé *isc-dhcp-server*.
Nous avons choisi ce dernier car il est parmi les plus répandus.
Voici un résumé de la configuration appliquée :

|        | Réseau          | Adresse Minimum | Adresse Maximum | Passerelle    |
|--------|-----------------|-----------------|-----------------|---------------|
| VLAN 2 | 192.168.10.0/24 | 192.168.10.20     | 192.168.10.240   | 192.168.10.254 |
| VLAN 3 | 192.168.20.0/24 | 192.168.20.10    | 192.168.20.240   | 192.168.20.254 |
| VLAN 4 | 192.168.30.0/24 | 192.168.30.10     | 192.168.30.240   | 192.168.30.254 |

et voici la configuration :
```
option domain-name "nethub-cgir.com";
option domain-name-servers ns1.nethub-cgir.com;


default-lease-time 600;
max-lease-time 7200;
allow booting;
allow bootp;

ddns-update-style interim;
server-identifier 192.168.10.12;
authoritative;

#VLAN 2

subnet 192.168.10.0 netmask 255.255.255.0 {
        range dynamic-bootp 192.168.10.20 192.168.10.240;
        option routers 192.168.10.254;
        option subnet-mask 255.255.255.0;
        option domain-name "nethub-cgir.com";
        option domain-name-servers 192.168.10.10;
}

#VLAN 3

subnet 192.168.20.0 netmask 255.255.255.0 {
        range dynamic-bootp 192.168.20.10 192.168.20.240;
        option routers 192.168.20.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 192.168.10.10;
}


#VLAN 4

subnet 192.168.30.0 netmask 255.255.255.0 {
        range dynamic-bootp 192.168.30.10 192.168.30.240;
        option routers 192.168.30.254;
        option subnet-mask 255.255.255.0;
        option domain-name-servers 192.168.10.10;
}

#Keys

key "dhcp-update-key" {
       algorithm hmac-sha256;
       secret "vvzEalTLMRSf+7CfJOZ70/WW/Y5Neep3tRGRaVnWKKk=";
};

zone nethub-cgir.com. {
primary 192.168.10.10;
key "dhcp-update-key";
}

zone 10.168.192.in-addr-arpa. {
primary 192.168.10.10;
key "dhcp-update-key";
}
```

Il y a une mise à jour automatique du serveur DNS, le serveur est autoritaire sur le réseau avec pour adresse IP *192.168.10.12*.

Il y a 2 pools d'adresses, celui de *VLAN 3 (Compta)* ainsi que celui du *VLAN 4 (Prod)* qui sont entres les adresses *192.168.X.X et 192.168.X.X* avec comme passerelle par défaut les sous-interfaces des VLAN correspondants *192.168.X.254*.

Ensuite on ajoute les zones du DNS ainsi que la clé rndc pour mettre à jour le DNS avec DHCP, ça peut notamment permettre de désigner les machines par des noms.

## DNS

<https://bind9.net/>

<img src="imgs/bind9.jpg" alt="dns" width="200"/>

Afin d'accèder au serveur web de façon plus simple pour les utilisteurs pas forcément à l'aise avec l'adressage IP, un DNS a été mis en place, c'est un protocole quasiment obligatoire. Le service est *bind9*.
C'est l'implémentation la plus répandue et elle est également libre.

Voici la configuration du *named.conf.local*
```
key "dhcp-update-key" {
       algorithm hmac-sha256;
       secret "vvzEalTLMRSf+7CfJOZ70/WW/Y5Neep3tRGRaVnWKKk=";
};

zone "nethub-cgir.com" {
        type master;
        file "nethub-cgir.com";
        allow-update {key "dhcp-update-key";};
};

zone "10.168.192.in-addr.arpa" {
        type master;
        file "nethub-cgir.com.rev";
        allow-update {key "dhcp-update-key";};
};

key "rndc-key" {
       algorithm hmac-sha256;
       secret "Brca3jzMeTa+xg2faCren2/Wz+1N+0ufYFA9+e/MfFg=";
};

controls {
        inet 127.0.0.1 port 953
               allow { 127.0.0.1; } keys { "rndc-key"; };

        inet 192.168.10.10 port 953
                allow { 192.168.10.12; } keys { "dhcp-update-key"; };
};

```

Les fichiers de zones ont été placés dans */var/cache/bind/*.
Ensuite la clé est renseignée, c'est la même que celle renseignée dans *dhcpd.conf*.

Zone *nethub-cgir.com* :
```
$ORIGIN nethub-cgir.com.
$TTL    604800
nethub-cgir.com. IN     SOA     ns1.nethub-cgir.com. root.nethub-cgir.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL

@                NS      ns1.nethub-cgir.com.

ns1             A       192.168.10.10

www             A       192.168.10.11
```
Et le fichier de zone inverse pour la résolution :

Zone *nethub-cgir.com.rev* :
```
$TTL    604800
@       IN      SOA     ns1.nethub-cgir.com. root.nethub-cgir.com. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL

@               NS      ns1.nethub-cgir.com.
$ORIGIN 10.168.192.in-addr.arpa.
10              PTR     ns1.nethub-cgir.com.
11              PTR     www.nethub-cgir.com.

```

## Mail

<http://www.postfix.org/>

![](imgs/mail.png)

Nous avons également tenter la mise en place d'un serveur mail afin de permettre aux différents utilisateurs de communiquer, nous avons utilisé *postfix* comme protocole de transmission de courriers électroniques. Puis, *Dovecot* comme serveur IMAP/POP3 qui gère les mails et *Thunderbird* comme client pour les utilisateurs.
Postfix est un serveur de messagerie STMP libre et répandu dans le domaine Linux.

Cependant, sa configuration n'a jamais aboutie.

## Serveur Web

<https://apache.org/>

Pour le serveur web nous avons décidé d'utiliser *Apache2* car il est open-source, il possède une communauté conséquente, très utile en cas de problème pour obtenir rapidement de l'aide.

Voici la page web, à laquelle nous pouvons accèder via le DNS en rentrant *nethub-cgir.com*.

![](imgs/site.png)

Voici la configuration du site web */etc/apache2/sites-available/nethub-cgir.com.conf*:

```
<VirtualHost *:80>
ServerAdmin webmaster@nethub-cgir.com
        DocumentRoot /var/www/html/
        ErrorLog ${APACHE_LOG_DIR}/error_nethub.log
        CustomLog ${APACHE_LOG_DIR}/access_nethub.log combined

        ServerName www.nethub-cgir.com
        ServerAlias nethub-cgir.com
        <Directory /var/www/html/>
                Options Indexes FollowSymLinks
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
```

Le fichier .html se trouve dans le dossier */var/www/html*.

# PC Clients

Grâce à tout cela mis en place, les PC Utilisateurs des deux entreprises prennent automatiquement leurs adresses IP et peuvent accéder aux pages Web respectives sans écrire d'addresses IP.

# Conclusion

En résumé, ce projet nous a permis de mettre en pratique les compétences que nous avions déjà acquises en classe, mais aussi d'en apprendre et de pratiquer des sujets qui n'étaient pas au programme.
Ce projet nous donne un aperçu de la mise en place d'un réseau au sein d'une entreprise et le fait de construire une organisation qui nous permettera d'optimiser notre temps lors de l'élaboration d'un projet.
Travailler en groupe est aussi une belle expérience pour partager des compétences et aborder des idées pour donner vie à ce projet. Ces mois de travail nous aurons également appris à mieux nous organiser, à adopter l’esprit d’équipe.
